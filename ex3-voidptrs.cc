#include <iostream>
void call(void(*fn)()) {
	(*fn)();	
}
int main() {
	auto fn = [](){ std::cout<<"hi\n"; };
	call(fn);
	
	auto fn2 = [](){ std::cout<<"hi2\n"; };
	call(fn2);
}
