#include <iostream>
void call(void(*fn)()) {
	(*fn)();	
}
void fn() { std::cout<<"hi\n"; }
void fn2() { std::cout<<"hi2\n"; }
int main() {
	call(fn);
	call(fn2);
}
