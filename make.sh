#!/usr/bin/bash

for x in *.cc; do
	name=${x%.*}
	for l in 0 1 2 3; do
		clang++ $x -std=c++11 -O$l -o $name-o$l &
	done
done
wait
strip *-o*
