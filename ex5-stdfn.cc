#include <functional>
#include <iostream>
void call(std::function<void(void)> fn) {
	fn();
}
int main() {
	auto fn = [](){ std::cout<<"hi\n"; };
	call(fn);
}
