#include <functional>
#include <iostream>
void call(std::function<void(void)> fn) {
	fn();
}
int main() {
	int x = 0;
	auto fn = [&](){ ++x; };
	call(fn);
	std::cout<<x<<'\n';
}
