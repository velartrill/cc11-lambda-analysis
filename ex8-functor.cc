#include <iostream>
struct functor {
	virtual void operator()() = 0;
};

struct fn : public functor {
	int* x;
	void operator()() override {
		(*x)++;
	}
};

void call(functor* f) {
	(*f)();
}

int main() {
	int x = 0;
	fn f;
	f.x = &x;
	call(&f);
	std::cout << x << '\n';
}
